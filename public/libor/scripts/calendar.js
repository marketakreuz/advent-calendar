let myCal = document.getElementById("adventCal");
let currentDate = new Date();
// Cislo mesice, ktereho se kalendar tyka



// Close modal
function modalClose() {
    document.getElementById("modal").style.display = "none";
};


function Door(calendar, day) {
    /*
        vykresleni okenek, ta ktera jsou jiz k dispozici, budou mit obrys.
    */

	this.width = ((calendar.width - 0.1 * calendar.width) / 4) * 0.95;
	this.height = ((calendar.height - 0.1 * calendar.height) / 6) * 0.95;
	this.adventMessage = messages[day - 1];
    this.dayNumber = day;
	this.x = ( 0.04 * calendar.width + ((day- 1) % 4) * (1.1 * this.width) );
	this.y = - ( 0.96 * calendar.height - Math.floor((day - 1) / 4) * (1.1 * this.height) );

	this.content = function() { 
		
		var node = document.createElement("li");
		document.getElementById("adventDoors").appendChild(node);
		node.id = "door" + day;
		node.style.cssText = "width: " + this.width + "px; height: " + this.height + "px; top: " + this.y + "px; left: " + this.x + "px;";

		var innerNode = document.createElement("a");
		document.getElementById("door" + day).appendChild(innerNode);
		innerNode.innerHTML = day;
		innerNode.href = "#";

		if( ( currentDate.getMonth() + 1 ) < monthInt || currentDate.getDate() < day ) {
			innerNode.className = "disabled";
			innerNode.onclick = function() {
				return false;
			}
		} else {
			var adventMessage = this.adventMessage;
            let dayNumber = this.dayNumber;
			innerNode.onclick = function() {
                // Toto se deje, kdyz kliknu na policko
                // document.getElementById("day").innerHTML = dayNumber;
                document.getElementById("modalText").innerHTML = adventMessage;
                document.getElementById("modal").style.display = "block";
				//alert(adventMessage);
				return false;
			}
		}	
	};

}

(function() {
	var doors = [];

	// set title
	document.title = title;
	document.getElementById("title").innerHTML = title;

	// set welcome message
	document.getElementById("message").innerHTML = welcomeMsg;

	// set image (nefunguje)
	// document.getElementById("adventCal").src = background_image;

	for(var i = 0; i < 24; i++) {

		doors[i] = new Door(myCal, i + 1);
		doors[i].content();

	}

	return doors;
})();
