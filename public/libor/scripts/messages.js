// ve kterem mesici se advent odpocitava (default je 12 pro prosinec)
const monthInt = 12;

// Image path (nefunguje)
// const background_image = "images/background.jpg";

// nadpis na strance (idealne do 50i znaku kuli formatovani)
const title = "Liborův adventní kalendář 2021";

// uvitaci text na titolni strance, delka muze byt libovolna
const welcomeMsg = "Uzij si svuj advent s tematickymi anglickymi vetami...";

// zprava na kazdy den, co radek to okenko.
const messages = [
    "The cookies for Santa are ___ the table, and the glass of milk is ___ the fridge.",
    "A: When can we open our presents? B: You can open them ___ 6 PM. But first, let’s eat our festive dinner ___ the kitchen.",
    "The carp is still ___ the oven, but it will be ready ___ 20 minutes.",
    "___ the beginning of the evening, the children were excited about their presents.",
    "Who did the Three Kings see ___ the stable __ Bethlehem?",
    "This year, we’re going to celebrate Christmas Eve ___ Friday.",
    "When Baby Jesus was born, he was put ___ a manger by his mother.",
    "Our family always goes on vacation ___ December because my mother hates cleaning the house for Christmas.",
    "Unfortunately, Santa got stuck ___ the chimney on the way down. He cannot get out ___ the moment.",
    "Santa will give you a ride. Would you like to sit ___ the deer or ride ___ Santa’s sleigh?",
    "The festive dinner was ready, but my father was stuck ___ a traffic jam. He did not arrive ___ time.",
    "Honey, I am not ___ home. I am ___ church helping the choir with the Christmas decorations.",
    "My little sister was sitting quietly and looking ___ the Nativity scene.",
    "Who doesn’t celebrate Christmas ___ the 24th of December?",
    "I cannot talk to you now. I am secretly wrapping presents for my husband ___ the moment.",
    "I bought all my Christmas presents ___ advance. Now, I can enjoy the season!",
    "Who knew that Baby Jesus was born ___ Bethlehem?", 
    "My mom always decorates the house for Christmas by putting snowflake stickers ___ our windows and Christmas lights ___ the roof.",
    "The magnificent Christmas tree was not placed outside the church, but it was put ___ the building.",
    "I woke up ___ 5 AM because I ate too many cookies on Christmas Eve, and I felt sick.",
    "The Star of Bethlehem was sitting ___ the roof of the stable where Baby Jesus was born.",
    "___ the past, people ___ England used to give gifts to the poor ___ Boxing Day.",
    "We will visit our relatives ___ Christmas, but not ___ the 25th of December.",
    "___ the Czech Republic, we bake special Christmas cookies___ the oven a few weeks before Christmas Eve."
]
